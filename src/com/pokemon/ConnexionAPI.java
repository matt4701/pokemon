package com.pokemon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONObject;

/**
 * Connexion à l'API de pokéApi url: https://pokeapi.co/
 * 
 * @author matthieu singainy
 *
 */
public class ConnexionAPI {
    private String url;

    public ConnexionAPI(String url) {
	super();
	this.url = url;
    }

    public URLConnection connexion() {
	try {
	    URLConnection yc = new URL(this.url).openConnection();
	    return yc;
	} catch (MalformedURLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return null;
    }

    public JSONObject objetPokemon() {
	URLConnection yc = connexion();
	BufferedReader in;
	try {
	    in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
	    StringBuilder sb = new StringBuilder();
	    String inputLine;
	    while ((inputLine = in.readLine()) != null)
		sb.append(inputLine);
	    JSONObject json = new JSONObject(sb.toString());
	    in.close();
	    return json;
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	return null;
    }

}
