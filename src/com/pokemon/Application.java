package com.pokemon;

import java.net.MalformedURLException;

/**
 * Lancer l'application Pokemon
 * 
 * @param args
 */
public class Application {

    public static void main(String[] args) throws MalformedURLException {
	// TODO Auto-generated method stub
	String urlPokemon = "https://pokeapi.co/api/v2/pokemon/";

	// Instancier Bulbizarre
	ConnexionAPI apiBulbizarre = new ConnexionAPI(urlPokemon + "1");
	Pokemon bulbizarre = new Pokemon(apiBulbizarre.objetPokemon());

	// Instancier Salameche
	ConnexionAPI apiSalameche = new ConnexionAPI(urlPokemon + "4");
	Pokemon salameche = new Pokemon(apiSalameche.objetPokemon());

	System.out.println("Que le combat Pokémon commence !");

    }
}
