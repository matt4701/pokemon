package com.pokemon;

import java.util.Random;

import org.json.JSONObject;

/**
 * Class pour instancier les pokémons
 * 
 * @author matthieu singainy
 *
 */
public class Pokemon {
    private JSONObject json;
    private String name;
    private int hp, baseExperience, attaque, defense;
    private Random rand;

    public Pokemon(JSONObject json) {
	super();
	this.json = json;
	this.name = json.get("name").toString();
	this.baseExperience = json.getInt("base_experience");
	JSONObject jsonHP = (JSONObject) json.getJSONArray("stats").get(0);
	this.hp = jsonHP.getInt("base_stat");
	JSONObject jsonAttaque = (JSONObject) json.getJSONArray("stats").get(1);
	this.attaque = jsonAttaque.getInt("base_stat");
	JSONObject jsonDefense = (JSONObject) json.getJSONArray("stats").get(2);
	this.defense = jsonDefense.getInt("base_stat");
    }

    public void attaquer(Pokemon pokemon) {
	int puissance = (this.attaque / 10) + (this.baseExperience / 30);
	if (puissance <= 0) {
	    rand = new Random();
	    puissance = rand.nextInt(5);
	}

	if (pokemon.hp > 0)
	    pokemon.hp -= puissance;
    }

    public void defendre(Pokemon pokemon) {
	int puissance = pokemon.attaque / (this.defense / 5);
	if (this.hp > 0)
	    this.hp -= puissance;
    }

    public int getAttaque() {
	return attaque;
    }

    public void setAttaque(int attaque) {
	this.attaque = attaque;
    }

    public int getDefense() {
	return defense;
    }

    public void setDefense(int defense) {
	this.defense = defense;
    }

    public JSONObject getJson() {
	return json;
    }

    public void setJson(JSONObject json) {
	this.json = json;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public int getBaseExperience() {
	return baseExperience;
    }

    public void setBaseExperience(int baseExperience) {
	this.baseExperience = baseExperience;
    }

    public int getHp() {
	return hp;
    }

    public void setHp(int hp) {
	this.hp = hp;
    }

    @Override
    public String toString() {
	return "Pokemon [name=" + this.name + "]";
    }
}
